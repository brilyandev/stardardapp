import axios from 'axios';
import StaticVar from '../Config/StaticVar';

// ===> api create 
const api = axios.create({
  baseURL: StaticVar.URL_GAMES,
  // timeout: 10000,
  headers:{}
});

// // ===> api interceptors 
// api.interceptors.request.use(function (config) {
//     // set headers after authentication
//     config.headers['token'] = localStorage.getItem("token");
//     return config;
// }, function (error) {
//   // Do something with request error
//   return Promise.reject(error);
// });

// ===> api list function request

const getContingenRequest = () => api.get('/contingents');
const getEventRequest = () => api.get('/events');
const getParticipantRequest = () => api.get('/participants')

export const apis = {
    getContingenRequest,
    getEventRequest,
    getParticipantRequest
}

export default apis