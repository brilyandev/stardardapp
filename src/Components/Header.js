import React from 'react'
import {
    AppBar,
    Toolbar,
    Typography,
    Button
} from "@material-ui/core";

import { NavLink } from "react-router-dom";

import useStyles from "./Styles";


export default function Header() {
    var classes = useStyles();

    return (
        <AppBar position="fixed" className={classes.appBar}>
            <Toolbar className={classes.toolbar}>
                <Button
                    component={NavLink} to="/"
                    className={classes.logoBtn}
                >
                    <Typography>L O G O</Typography>
                </Button>
                <Button exact component={NavLink} to="/">
                    <Typography>Cabang Olahraga</Typography>
                </Button>
                <Button exact component={NavLink} to="/participants">
                    <Typography>Participant</Typography>
                </Button>
                <Button exact component={NavLink} to="/contingents">
                    <Typography>Kontingent</Typography>
                </Button>
                <Button exact component={NavLink} to="/">
                    <Typography>Menu 4</Typography>
                </Button>
            </Toolbar>
        </AppBar>
    )
}
