import React from 'react'

import { Grid,Button,Typography } from '@material-ui/core';

import { NavLink } from "react-router-dom";
import useStyles from "./Styles";

export default function Footer() {
    var classes = useStyles();

    return (
        <>
            <div className={classes.footerContainer}>
                <Grid container>
                    <Button
                        component={NavLink} to="/"
                        className={classes.logoBtn}
                    >
                        <Typography>L O G O</Typography>
                    </Button>
                </Grid>
            </div>
        </>
    )
}
