import { makeStyles } from "@material-ui/styles";

export default makeStyles(theme => ({
  appBar: {
    minWidth:200,
    width: "100%",
    height:70,
    backgroundColor: "#fff",
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  toolbar: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2)
  },
  logoBtn: {
    width: 155,
    height:70,
    borderLeft:"1px solid gray",
    borderRight:"1px solid gray",
    borderRadius:0,
    backgroundColor: 'transparent',
    marginLeft:40,
    marginRight:40
  },
  footerContainer:{
      width:"100%",
      height:500,
      paddingTop:50,
      paddingLeft:20,
      paddingRight:20,
      backgroundColor:"#fff"
  }
}));
