import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {EventProvider} from "./Context/EventContext"
import ContingentProvider from "./Context/ContingentContext"
import MultiProvider from "./Config/MultiProvider"
import { ThemeProvider } from "@material-ui/styles";
import { CssBaseline } from "@material-ui/core";
import Themes from "./Themes";

ReactDOM.render(
  <MultiProvider
    providers={[
      <EventProvider />,
      <ContingentProvider />,
    ]}>
    <ThemeProvider theme={Themes.default}>
    <CssBaseline />
    <App />
    </ThemeProvider>
  </MultiProvider>,
        
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
