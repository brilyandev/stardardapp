
const metrics = {
  marginHorizontal: 10,
  marginVertical: 60,
  section: 25,
  baseMargin: 10,
  doubleBaseMargin: 20,
  smallMargin: 5,
  paddingTopApp:10,
  paddingBottomApp:10,
  paddingLeftApp:14,
  paddingRightApp:14,
  doubleSection: 50,
  horizontalLineHeight: 1,
  buttonRadius: 4,
  icons: {
    tiny: 15,
    small: 20,
    medium: 30,
    large: 45,
    xl: 50
  },
  images: {
    small: 20,
    medium: 40,
    large: 60,
    logo: 200
  }
}

export default metrics
