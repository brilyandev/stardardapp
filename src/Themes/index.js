import Colors from './Colors'
import Fonts from './Fonts'
import Metrics from './Metrics'
import Images from './Images'


import defaultTheme from "./default";

import { createMuiTheme } from "@material-ui/core";

export default {
  default: createMuiTheme({ ...defaultTheme }),
  Images: {...Images},
  Colors: {...Colors},
  Metrics: {...Metrics}
};
