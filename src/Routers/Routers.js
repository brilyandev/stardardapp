import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Route, Switch, withRouter } from "react-router-dom";

import Home from "../Pages/Home/Home";
import Participant from '../Pages/Participants/Participant';
import Contingent from '../Pages/Contingents/Contingent';
import Header from '../Components/Header';
import Footer from '../Components/Footer';
import { Box } from '@material-ui/core';

function Routers(props) {

    return (
        <div>
            <Router>
                <Header/>
                <Box mb={10}/>
                <Route exact path="/" component={Home} />
                <Route exact path="/participants" component={Participant} />
                <Route exact path="/contingents" component={Contingent} />
            </Router>
        </div>
    );
}

export default Routers;