import React, { createContext, useReducer } from "react";
import StaticVar from "../Config/StaticVar";
import Api from "../Services/Api";
var EventContext = createContext();

function EventReducer(events, action) {
  switch (action.type) {
    case 'GET_DATA': 
      return [...action.payload];
    case "SEARCH_DATA":
      return [...action.modifiedpayload];
    case "POST_DATA":
      return [...events, action.payload];
    case "PUT_DATA":
      return events.map(x=>{if(x._id===action.payload._id){return action.payload}else{return x}})
    case "DELETE_DATA":
      return events.filter(item => item._id !== action.id);
    case "GET_DATA_ERROR":
      return [...events, []];
    default: {
        throw new Error(`Unhandled action type: ${action.type}`);
      }   
  }
}

function EventProvider({ children }) {

  var [events, eventdispatch] = useReducer(EventReducer, []);

  return (
    <EventContext.Provider value={{ events, eventdispatch }}>
      {children}
    </EventContext.Provider>
  );
}

function getData(events,eventdispatch) {
    if(events.length === 0){
      Api.getEventRequest().then((res)=>{
        eventdispatch({type:"GET_DATA", payload: res.data})
      })
    }
}

function postData(eventdispatch) {
  Api.getEventRequest().then((res)=>{
    eventdispatch({type:"SET_DATA", payload: res.data})
  })
}

function putData(eventdispatch) {
  Api.getEventRequest().then((res)=>{
    eventdispatch({type:"SET_DATA", payload: res.data})
  })
}

function searchData(eventdispatch) {
  Api.getEventRequest().then((res)=>{
    eventdispatch({type:"SET_DATA", payload: res.data})
  })
}

function deleteData(eventdispatch) {
  Api.getEventRequest().then((res)=>{
    eventdispatch({type:"SET_DATA", payload: res.data})
  })
}

export { EventContext, EventProvider, getData };