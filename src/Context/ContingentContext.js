import React, { createContext, useReducer, useState } from "react";
import StaticVar from "../Config/StaticVar";
import Api from "../Services/Api";
export const ContingentContext = createContext();

const localcontingent = JSON.parse(localStorage.getItem("contingents"))

function ContingentReducer(Contingents, action) {
  switch (action.type) {
    case 'SET_DATA': 
      return [...action.payload];
      case "SEARCH_DATA":
        return [...action.modifiedpayload];
      case "POST_DATA":
        return [...Contingents, action.payload];
      case "PUT_DATA":
        return Contingents.map(x=>{if(x._id===action.payload._id){return action.payload}else{return x}})
      case "DELETE_DATA":
        return Contingents.filter(item => item._id !== action.id);
      case "GET_DATA_ERROR":
        return [...Contingents, []];
      default: {
          throw new Error(`Unhandled action type: ${action.type}`);
        } 
  }
}

function ParticipantReducer(Participants, action) {
  switch (action.type) {
    case 'SET_DATA': 
      return [...action.payload];
      case "SEARCH_DATA":
        return [...action.modifiedpayload];
      case "POST_DATA":
        return [...Participants, action.payload];
      case "PUT_DATA":
        return Participants.map(x=>{if(x._id===action.payload._id){return action.payload}else{return x}})
      case "DELETE_DATA":
        return Participants.filter(item => item._id !== action.id);
      case "GET_DATA_ERROR":
        return [...Participants, []];
      default: {
          throw new Error(`Unhandled action type: ${action.type}`);
        } 
  }
}

function ContingentProvider({ children }) {

  var [contingents, contingentdispatch] = useReducer(ContingentReducer, localcontingent || [])
  var [participants, participantdispatch] = useReducer(ParticipantReducer, [])
  const [detailcontingent, setdetailcontingent] = useState({})
  
  function getData(){
    if(contingents.length === 0){
      Api.getContingenRequest().then((res)=>{
        contingentdispatch({type:"SET_DATA", payload: res.data})
        localStorage.setItem('contingents', JSON.stringify(res.data))
      }
      )
    }
  }

  function putdatacontingent(data){
    contingentdispatch({type:"PUT_DATA", payload: data})
    localStorage.setItem('contingents', JSON.stringify(contingents))
  }

  function getDataDetail(id){
    setdetailcontingent(contingents.filter(x=>x._id === id)[0])
  }

  function getDataParticipant(){
    if(participants.length === 0){
      Api.getEventRequest().then((res)=>{participantdispatch({type:"SET_DATA", payload: res.data})})
    }
  }

  return (
    <ContingentContext.Provider value={{ contingents, participants, getData, getDataParticipant, detailcontingent, getDataDetail, putdatacontingent }}>
      {children}
    </ContingentContext.Provider>
  );
}

export default ContingentProvider;