import React, {useContext, useEffect, useState} from 'react'
import Api from "../../Services/Api"
import {EventContext,getData} from "../../Context/EventContext"
import { ContingentContext } from '../../Context/ContingentContext'
export default function Home(props) {
    // const [events, setEvents] = useState([])
    
    const { events,eventdispatch }= useContext(EventContext)
    const { detailcontingent }= useContext(ContingentContext)
    useEffect(() => {
        getData(events,eventdispatch)
    }, [])

    return (
        <div>
            <p>Home Page</p>
            {/* <button onClick={()=>props.history.push('/')}>Home</button>
            <button onClick={()=>props.history.push('/schedules')}>Schedules</button>
            <button onClick={()=>props.history.push('/contingents')}>Contingent</button> */}
            <ul>
            {
                    events.map((item,index)=>{
                        return(<li key={item._id}>{index+1}. {item.event_name}</li>)
                    })
            }
            </ul>

            <p>Detail Kontingen</p>
            <p>
                {
                    JSON.stringify(detailcontingent)
                }
            </p>
        </div>
    )
}
