import React, {useContext,useEffect} from 'react'
import { ContingentContext } from '../../Context/ContingentContext'

export default function Contingent(props) {
    const { contingents, getData, detailcontingent, getDataDetail, putdatacontingent  }= useContext(ContingentContext)

    useEffect(() => {
        getData()
    }, [])
    return (
        <div>
            <p>Contingent Page</p>
            {/* <button onClick={()=>props.history.push('/')}>Home</button>
            <button onClick={()=>props.history.push('/schedules')}>Schedules</button>
            <button onClick={()=>props.history.push('/contingents')}>Contingent</button>          */}
            <ul>
                {
                    contingents.map((item,index)=>{
                    return(
                        <li key={item._id}>{index+1}. {item.name}
                            <button onClick={()=>getDataDetail(item._id)}>Lihat Detail</button>
                            <button onClick={()=>putdatacontingent(
                                {
                                    ...item, name:'Dani'
                                }
                            )}>Lihat Detail</button>
                        </li>
                    )})
                }
            </ul>

            <p>Detail Kontingen</p>
            <p>
                {
                    JSON.stringify(detailcontingent)
                }
            </p>
        </div>
    )
}

