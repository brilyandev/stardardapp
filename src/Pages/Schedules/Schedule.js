import React, {useContext, useEffect, useState} from 'react'
import Api from "../../Services/Api"
import {EventContext,getData} from "../../Context/EventContext"

export default function Schedule(props) {
    const { events,eventdispatch }= useContext(EventContext)
    
    useEffect(() => {
        getData(events,eventdispatch)
    }, [])

    return (
        <div>
            <p>Schedules</p>
            {/* <button onClick={()=>props.history.push('/')}>Home</button>
            <button onClick={()=>props.history.push('/schedules')}>Schedules</button>
            <button onClick={()=>props.history.push('/contingents')}>Contingent</button> */}
            <ul>
            {
                    events.map((item,index)=>{
                        return(<li key={item._id}>{index+1}. {item.event_name}</li>)
                    })
            }
            </ul>

            
        </div>
    )
}
