import React, {useContext,useEffect} from 'react'
import { ContingentContext } from '../../Context/ContingentContext'
export default function Participant() {
    const { participants, getDataParticipant }= useContext(ContingentContext)

    useEffect(() => {
        getDataParticipant()
    }, [])
    return (
        <div>
            <p>Participant Page</p>
            {/* <button onClick={()=>props.history.push('/')}>Home</button>
            <button onClick={()=>props.history.push('/schedules')}>Schedules</button>
            <button onClick={()=>props.history.push('/contingents')}>Contingent</button>          */}
            <ul>
                {
                    participants.map((item,index)=>{
                    return(
                        <li key={item._id}>{index+1}. {item.event_name}</li>
                    )})
                }
            </ul>
        </div>
    )
}
